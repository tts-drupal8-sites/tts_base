/**
 * @file
 * Global utilities.
 *
 */
(function($, Drupal) {

  'use strict';

  $('a').each(function() {
    if ($(this).find('img').length) {
      $(this).addClass('img-rollover')
    }
  });

  Drupal.behaviors.setAttributes = {
    attach: function (context, settings) {
      // remove anchor tags if an editor adds a link to a media embed  other than img
      // Select all anchor tags that contain a div with data-entity-type="media" but not containing an img tag
      $('a').has('div[data-entity-type="media"]').not(':has(img)').each(function() {
      // Unwrap the anchor tag, leaving the inner HTML intact
        $(this).contents().unwrap();
      });
      // Replace 'your-element-selector' with the appropriate selector for your element.
      var $element = $('#off-canvas', context);
  
      if ($element.length > 0) {
        $element.attr('role', 'navigation');
        $element.attr('aria-label', 'mobile-menu');
      }
      
      $("a[target='_blank']").append('<span class="sr-only"> (link opens in a new window/tab)</span>');
  
      // Define menuItems
      var menuItems = $('.menu__item--has-submenu', context);

      // Loop through each menu item
      for (var i = 0; i < menuItems.length; i++) {
        // Select the toggle button of the current menu item
        var toggle = menuItems[i].querySelector('.menu__submenu-toggle');

        // Add click and focus event listeners to the toggle button
        toggle.addEventListener('click', function(event) {
          // Prevent the default action
          event.preventDefault();

          // Toggle the submenu
          toggleSubmenu(this);
        });

        toggle.addEventListener('focus', function(event) {
          // Toggle the submenu
          toggleSubmenu(this);
        });

        // Add mouseover event listener to each menu item
        menuItems[i].addEventListener('mouseover', function(event) {
          // Select the submenu of the current menu item
          var submenu = this.querySelector('.menu__submenu-container');

          // Display the submenu
          submenu.style.display = 'block';
        });

        // Add mouseout event listener to each menu item
        menuItems[i].addEventListener('mouseout', function(event) {
          // Select the submenu of the current menu item
          var submenu = this.querySelector('.menu__submenu-container');
          var toggle = this.querySelector('.menu__submenu-toggle');

          // Only hide the submenu if the toggle button has not been clicked
          if (toggle.getAttribute('aria-expanded') === 'false') {
          //  submenu.style.display = 'none';
          }
        });
      }

      function toggleSubmenu(toggle) {
        // Select the submenu of the current menu item
        var submenu = toggle.nextElementSibling;

        // Check if submenu is not null before accessing its style property
        if (submenu) {
          // Toggle the display of the submenu and the 'aria-expanded' attribute
          if (toggle.getAttribute('aria-expanded') === 'false') {
            submenu.style.display = 'block';
            toggle.setAttribute('aria-expanded', 'true');
          } else {
            submenu.style.display = 'none';
            toggle.setAttribute('aria-expanded', 'false');
          }
        }
      }

    }
  };

})(jQuery, Drupal);
